using Microsoft.VisualStudio.TestTools.UnitTesting;
using Helper;
using System;

namespace HelperTests
{
    [TestClass]
    public class UnitTest1
    {
        //Test the Payment helper class
        [TestMethod()]
        public void PopulatePaymentObject()
        {
            const double rate = 4;
            const double term = 1;
            const double principle = 120000;
            Payment payment = new Payment(rate, term, principle);

            Assert.IsTrue(payment.Amortizations.Count.Equals(Convert.ToInt32(term * 12)));
        }
    }
}
