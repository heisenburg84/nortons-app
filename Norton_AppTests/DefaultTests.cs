﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using Helper;

namespace Norton_App.Tests
{
    [TestClass()]
    public class DefaultTests
    {
        //Test the build_DDL method
        [TestMethod()]
        public void BuildDropDownList()
        {
            DropDownList ddlTest = new DropDownList();
            var test = new Default();

            for (int i = 0; i < 5; i++)
            {
                ddlTest.Items.Add(test.Build_DDL(i));
            }

            Assert.IsTrue(ddlTest.Items.Count.Equals(5));
        }

        //Test the Payment helper class
        [TestMethod()]
        public void PopulatePaymentObject()
        {
            const double rate = 4;
            const double term = 1;
            const double principle = 120000;
            Payment payment = new Payment(rate, term, principle);

            Assert.IsTrue(payment.Amortizations.Count.Equals(Convert.ToInt32(term * 12)));
        }

        //Test the Amortization helper class
        [TestMethod()]
        public void SerializePaymentJSON()
        {
            const double rate = 4;
            const double term = 5;
            const double principle = 120000;
            Payment payment = new Payment(rate, term, principle);

            string strTempList = JsonConvert.SerializeObject(payment.Amortizations);
            List<Amortization> lstTestList = JsonConvert.DeserializeObject<List<Amortization>>(strTempList);

            Assert.IsTrue(lstTestList.Count.Equals(Convert.ToInt32(term * 12)));
        }

        [TestMethod()]
        public void GetASyncDataTest()
        {
            const string rate = "4";
            const string term = "5";
            const string principle = "120000";
            const string failRate = "bad data";

            string strTest = Default.GetASyncData(rate, term, principle);
            List<Amortization> lstTest = JsonConvert.DeserializeObject<List<Amortization>>(strTest);

            strTest = Default.GetASyncData(failRate, term, principle);

            Assert.AreEqual(lstTest.Count, int.Parse(term) * 12);
            Assert.AreEqual(strTest, string.Empty);
        }

        [TestMethod()]
        public void GetPaymentsTest()
        {
            const double rate = 4;
            const double term = 5;
            const double principle = 120000;

            Payment payment = new Payment(rate, term, principle);
            bool testFunction = payment.GetPayments();

            Assert.IsTrue(testFunction);
        }

        [TestMethod()]
        public void SetDDLTest()
        {
            var test = new Default();
            DropDownList ddlTest = test.Set_DDL(0, 9);
            Assert.AreEqual(10, ddlTest.Items.Count);

            for (int i = 0; i < ddlTest.Items.Count; i++)
            {
                Assert.AreEqual(i, int.Parse(ddlTest.Items[i].Value));
            }
        }
    }
}