﻿using Elmah;
using Helper;
using Newtonsoft.Json;
using System;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Norton_App
{
    public partial class Default : Page
    {
        public void Page_Load(object sender, EventArgs e)
        {
            if (!IsCallback)
            {
                this.ddlAnnualInterestRate = Set_DDL(2, 10);
                this.ddlTermYears = Set_DDL(1, 30);
            }
        }

        [WebMethod]
        public static string GetASyncData(string strRate, string strTerm, string strPrinciple)
        {
            //Initialize values
            double rate = 0;
            double term = 0;
            double principle = 0;

            //Validate input and set initial values.
            try
            {
                rate = double.Parse(strRate);
                term = double.Parse(strTerm);
                principle = double.Parse(strPrinciple);
            }
            catch (Exception ex)
            {
                ErrorLog.GetDefault(HttpContext.Current).Log(new Error(ex));
                return string.Empty;
            }

            //Create payment object by initializing the payment helper class.
            Payment pmtPayment = new Payment(rate, term, principle);

            //Return JSON string object
            return JsonConvert.SerializeObject(pmtPayment.Amortizations);
        }

        //Set drop down list.
        public DropDownList Set_DDL(int intInitializeLoop, int intStopLoop)
        {
            DropDownList ddlOutput = new DropDownList();

            //Build the Annual Interest Rate (%) drop down list, initialized at 2% interest
            for (int i = intInitializeLoop; i <= intStopLoop; i++)
                ddlOutput.Items.Add(Build_DDL(i));

            return ddlOutput;
        }

        //Function to build the drop down lists.
        public ListItem Build_DDL(int num)
        {
            //Create ListItem object, add text and value, and return object to reference.
            ListItem liItem = new ListItem
            {
                Text = (num).ToString(),
                Value = (num).ToString()
            };
            return liItem;
        }
    }
}