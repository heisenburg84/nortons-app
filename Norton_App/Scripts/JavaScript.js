﻿/// <reference path="jquery-3.3.1.intellisense.js"/>
/// <reference path="https://cdn.datatables.net/v/dt/dt-1.10.15/datatables.min.js" />

$(document).ready(function () {
    //Load JS for events that need to fire when the page loads.
    console.log("Page ready.");

    $("#btnSubmit").click(function () {
        console.log("Button Click");

        //Validate the data.
        if (!validation()) {
            console.log("Failed page validation.");
            return false;
        }

        //Create and send data to middleware for analysis
        var input = JSON.stringify(
            {
                strRate: $('#ddlAnnualInterestRate').val(),
                strTerm: $('#ddlTermYears').val(),
                strPrinciple: $('#txtPrinciple').val()
            });
        var url = 'Default.aspx/GetASyncData';
        var callback = OnSuccess;

        getData(input, url, callback);
    });
});

function WCFSuccess(response) {
    console.log("WCF Transfer Successful.");
    var success = JSON.parse(response);
    console.log("Processing Result: " + success.isValid);
}

//Different validations that your page may need.
function validation() {
    var principle = $('#txtPrinciple').val();

    //Check for no value in the principle input box.
    if (principle === "") {
        console.log("Please enter a value for the principle.");
        return false;
    }

    //Check data type for value in the principle input box.
    if (isNaN(principle)) {
        console.log("Please make sure the principle is a valid number.");
        return false;
    }

    //If all validation passes return true.
    return true;
}

//Gets the data from the C# middleware dll.
function getData(data, url, callback) {

    //Ajax connects to the middleware, jQuery has an implementation for Ajax
    //The data sent to the server is converted to a JSON object.  This makes it easier to parse later.
    //Check the Javascript console in the page for the error logs.
    $.ajax({
        type: 'POST',
        url: url,
        data: data,
        contentType: 'application/json',
        dataType: 'json',
        error: function (jqXHR, textStatus, errorThrown) {
            console.log("JSON Error, Aborting Process...");
            console.log("Status:", textStatus);
            console.log("Error Thrown: ", errorThrown);
            console.log("Details:", jqXHR);
        },
        beforeSend: function () {
            console.log("Starting JSON Transfer...");
        },
        success: callback,
        failure: function (response) {
            console.log("Failed transmission of JSON data.");
            console.log(response.d);
        }
    });

    return false;
}

//Process the successful return of the payment JSON object.
function OnSuccess(response) {
    console.log("Successful transmission of JSON data.");
    console.log(JSON.parse(response.d));
    updateDataTable(response.d);
}

//The data gets appended to the table by the DataTables plugin.
function updateDataTable(data) {
    var objJSON = JSON.parse(data);

    //Build table and append HTML to update div.
    var elementName = 'tblResult';
    if ($("#" + elementName).length === 0) {
        var tableHTML = '<table id="' + elementName + '" class="display table table-striped table-bordered">' +
            '<thead>' +
            '<tr>' +
            '</tr>' +
            '</thead>' +
            '<tbody>' +
            '<tr></tr>' +
            '</tbody>' +
            '</table><br style="clear: both"/>';

        $("#pnlUpdate").append(tableHTML);
    }

    var keys = [];
    for (var key in Object(objJSON[0]))
        keys.push({ 'data': key, 'title': key });

    //DataTables initialization
    $('#' + elementName).DataTable({
        'destroy': true,
        'pageLength': 12,
        'data': objJSON,
        'columns': keys
    });
}
