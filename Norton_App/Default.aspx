﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Norton_App.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Norton's App</title>
    <link href="Content/Site.css" rel="stylesheet" />
    <link href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css" rel="stylesheet" />
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js" type="text/javascript"></script>
    <script src="Scripts/JavaScript.js" type="text/javascript"></script>
</head>
<body>
    <form id="frmMain" runat="server">
        <div id="body">
            <div class="body-content">
                <div>
                    Principle<br />
                    <asp:TextBox ID="txtPrinciple" runat="server"></asp:TextBox>
                </div>
                <div>
                    Annual Interest Rate (%)<br />
                    <asp:DropDownList ID="ddlAnnualInterestRate" runat="server"></asp:DropDownList>
                </div>
                <div>
                    Term Years<br />
                    <asp:DropDownList ID="ddlTermYears" runat="server"></asp:DropDownList>
                </div>
            </div>
            <div>
                <div>
                    Monthly Payment:&nbsp
            <asp:Label ID="lblMonthlyPayment" runat="server" Text=""></asp:Label>
                </div>
                <div>
                    <asp:Button ID="btnSubmit" runat="server" Text="Calculate" OnClientClick="return false;" />
                </div>
            </div>
            <div>
                <%-- Where the magic happens --%>
                <asp:Panel ID="pnlUpdate" CssClass="table_spacing" runat="server">
                </asp:Panel>
            </div>
        </div>
    </form>
</body>
</html>
