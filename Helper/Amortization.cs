﻿using System;

namespace Helper
{
    public class Amortization
    {
        //Private variables
        private string _MonthlyPayment;
        private string _Interest;
        private string _Principle;
        private string _LoanBalacnce;

        //Public accessible object properties
        //Each property is formatted to take in a string and convert it to a currency string format.
        //The property needs to convert the string to a decimal before it can do the currency string covert.
        //In the test scripts I have to de-serialize a list I created, to get that to pass I needed to
        //add '.Replace("$","")' to the input value because the list is fed in formatted already.
        public string MonthlyPayment
        {
            get { return string.Format("{0:C}", Convert.ToDecimal(_MonthlyPayment)); }
            set { _MonthlyPayment = value.Replace("$",""); }
        }

        public string Interest
        {
            get { return string.Format("{0:C}", Convert.ToDecimal(_Interest)); }
            set { _Interest = value.Replace("$", ""); }
        }

        public string Principle
        {
            get { return string.Format("{0:C}", Convert.ToDecimal(_Principle)); }
            set { _Principle = value.Replace("$", ""); }
        }

        public string LoanBalance
        {
            get { return string.Format("{0:C}", Convert.ToDecimal(_LoanBalacnce)); }
            set { _LoanBalacnce = value.Replace("$", ""); }
        }
    }
}
