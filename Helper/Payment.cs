﻿using System;
using System.Collections.Generic;

namespace Helper
{
    public class Payment
    {
        //Private Variables
        private double _Rate { get; }
        private double _NPer { get; }
        private double _PV { get; }

        //Public accessible object properties
        public List<Amortization> Amortizations { get; set; }

        //Constructor for main process
        public Payment(double rate, double nper, double pv)
        {
            _Rate = rate;
            _NPer = nper;
            _PV = pv;
            this.Amortizations = new List<Amortization>();

            GetPayments();
        }

        //Get payment data
        public bool GetPayments()
        {
            double dblEndBalance, dblRate, dblPayment, dblFactor, dblLoanTerm;

            //Amortization calculations done here.
            dblLoanTerm = _NPer * 12;
            dblEndBalance = _PV;
            dblRate = _Rate / 12 / 100;
            dblFactor = dblRate + (dblRate / (Math.Pow(dblRate + 1, dblLoanTerm) - 1));
            dblPayment = _PV * dblFactor;

            //Loop through the term of the loan and calculate monthly values.
            for (int i = 0; i < dblLoanTerm; i++)
            {
                //Initialize Amortization object and add values
                Amortization objAmortization = new Amortization
                {
                    LoanBalance = Math.Round(dblEndBalance, 2).ToString(),
                    MonthlyPayment = Math.Round(dblPayment, 2).ToString(),
                    Interest = String.Format("{0}", dblEndBalance * dblRate),
                    Principle = String.Format("{0}", Math.Round(dblPayment - (dblEndBalance * dblRate), 2))
                };

                //Add Amortization object to the Amortizations property.
                Amortizations.Add(objAmortization);

                //Track the balance of the month we are in.
                dblEndBalance -= Math.Round(dblPayment - (dblEndBalance * dblRate), 2);
            }

            return true;
        }
    }
}
